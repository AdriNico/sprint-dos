const mongoose = require('../conexion');
const express = require('express');
const app = express();
const port = 3025;
const host = 'http://localhost';
const url = `${host}:${port}`;
const { Schema } = mongoose;



app.use(express.urlencoded({ extended: true }));
app.use(express.json());



const schemaUser = new Schema({
   nombre: String,
   apellido: String,
   email: String,
   saldo: Number
});

const userModel = mongoose.model('usuarios', schemaUser);

app.post('/', (req, res) => {

    const createUser = async () => {
  
      let userToBeCreated = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        email: req.body.email,
        saldo: req.body.saldo
      }
  
      const newUser = new userModel(userToBeCreated);
      const result = await newUser.save();
      return result;
    }
  
    const response = createUser();
  
    response
    .then(result => res.json(result))
    .catch(err => res.json(err));
  
  });
  
app.post('/saldo'), (req,res) => {

    const modificarSaldo = async () => {

        let ModificarSaldo = {
            email: req.body.email,
            saldo: req.body.saldo
        }

        const newSaldo = new userModel(newSaldo);
        const result = await newSaldo.save();
        return result;
    }

    const response = modificarSaldo();

    response
    .then(result => res.json(result))
    .catch(err => res.json(err));
    };


app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
})

