const express = require('express');
const app = express();
// Setup server port
const port = process.env.PORT ||3029;
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.get('/', (req, res) => {
  res.send("Hello World");
});