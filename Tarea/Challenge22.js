const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:3022/challenge');

const Usuario = db.model('User', new Schema({ nombre: string }));

const session = await db.startSession();
session.startTransaction();

await Usuario.create([{ nombre: 'Test'}], { session: session});

let doc = await Usario.findOne({ nombre: 'Test'});
assert.ok(!doc);

doc = await Usuario.findOne({ nombre: 'Test'}).session(session);
assert.ok(doc);

await session.commitTransaction();
doc = await Usuario.findOne({ nombre: 'Test'});
assert.ok(doc);

session.endSession();