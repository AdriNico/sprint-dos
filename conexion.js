require('dotenv').config();
const mongoose = require('mongoose');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

//const MONGODB_HOST = process.env.MONGODB_HOST;
//const MONGODB_PORT = process.env.MONGODB_PORT;


const conectionString = `mongodb://localhost:27017/`;

mongoose.connect(conectionString, options);

module.exports = mongoose;