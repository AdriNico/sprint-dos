--
-- Base de datos: `id15380665_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes`
--

CREATE TABLE `estudiantes` (
  `id_estudiante` int(4) NOT NULL,
  `nombre_estudiante` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `apellido_estudiante` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email_estudiante` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono_estudiante` bigint(12) NOT NULL,
  `fecha_alta` date NOT NULL DEFAULT current_timestamp(),
  `edad_estudiante` int(2) NOT NULL,
  `fecha_nacimiento_estudiante` date NOT NULL,
  `es_casada` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estudiantes`
--

INSERT INTO `estudiantes` (`id_estudiante`, `nombre_estudiante`, `apellido_estudiante`, `email_estudiante`, `telefono_estudiante`, `fecha_alta`, `edad_estudiante`, `fecha_nacimiento_estudiante`, `es_casada`) VALUES
(1, 'Cristian', 'Villareal', 'Ctian.vi1989@gmail.com', 54911234, '2021-09-14', 30, '1992-09-13', 0),
(3, 'Matias', 'Parache', 'iasparache@pm.me', 54911234, '2021-09-14', 35, '1986-07-26', 1),
(4, 'Santiago', 'bernal', 'ctlimon@gmail.com', 54911234, '2021-09-13', 31, '1990-02-28', 1),
(5, 'Gustavo', 'Sobol', 'tavo.javier.sobol@gmail.com', 54911234, '2021-09-13', 39, '1982-01-20', 1),
(6, 'Leonel', 'Rivera', 'lel@gmail.com', 54911234, '2021-09-13', 31, '1988-05-25', 1),
(7, 'Enzo', 'Melian', 'o.melian@gmail.com', 54911234, '2021-09-13', 20, '2001-05-24', 0),
(9, 'Leandro', 'Suasnabar', 'lnabar@gmail.com', 54911234, '2021-09-13', 30, '1991-04-02', 1),
(10, 'Santiago', 'Barreto', 'agonicolasbarreto@gmail.com', 54911234, '2021-04-07', 18, '2003-04-07', 0),
(12, 'Javier', 'Oyarzo', '14_228@hotmail.com', 54911234, '2021-09-13', 31, '1990-04-01', 0),
(14, 'Daniel', 'Jerez', '85@gmail.com', 54911234, '2021-09-13', 36, '1985-07-07', 1),
(17, 'Adriel', 'Gomez', 'ielnicolas@hotmail.com', 542964490061, '2021-09-13', 31, '1999-07-25', 1),
(18, 'Marcelo', 'Cussi', '0000@hotmail.com', 541112345677, '2021-09-13', 31, '1978-09-22', 0),
(19, 'Bruno', 'Bonasif', 'nvb@gmail.com', 54911234, '2021-09-21', 26, '1995-04-27', 0),
(20, 'Alejandro', 'Noriega', 'ega76@gmail.com', 54911234, '2021-09-13', 43, '1976-08-11', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estudiantes`
--
ALTER TABLE `estudiantes`
  MODIFY `id_estudiante` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;
