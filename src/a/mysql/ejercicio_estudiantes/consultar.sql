-- Las personas mayores de edad (18 años)
SELECT * FROM `estudiantes` WHERE fecha_nacimiento_estudiante < '2003-09-13'

-- Las personas casadas
SELECT * from estudiantes where es_casada = 1

-- Las personas que tengan su email terminado en @google.com
SELECT * FROM `estudiantes` WHERE email_estudiante like '%@gmail%'

-- Las personas que NO tengan su email terminado en @google.com
SELECT * FROM `estudiantes` WHERE email_estudiante not like '%@gmail%'